Wenger Cookbook
--

This is the LaTeX source code and build scripts for the Wenger Cookbook.

To compiled this source into .pdf format requires the LaTeX tool and the cuisine
package.



To install the required software on Ubuntu:

    apt-get install texlive-latex-base texlive-latex-extra



To build a pdf in Ubuntu just run `make` which will run the`pdflatex` step twice
and will generate the table of contents too.


To install the required software in windows download and install Miktex. After
installation run the updater until everything is up to date. If using Sublime
Text install the latex-tools package, and run migration from the command pallet.
This will install the build system, and allow you to use `ctrl-b` to build. Note
that Sublime Text can be set up this way (with texlive instead of Miktex) in
linux.
