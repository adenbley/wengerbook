all:
	./compile.py
	pdflatex wengercookbook.tex
	pdflatex wengercookbook.tex
	pdflatex wengercookbook.tex

clean:
	-rm *.out *.toc *.aux *.log
