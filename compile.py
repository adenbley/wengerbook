#! /usr/bin/env python

"""This script will build the file wengercookbook from the directories and their
sub directories"""


import os

def docstart():
    "returns the preamble of the tex document"

    return r"""\documentclass[a5paper,10pt,twoside]{article}
\usepackage[a5paper]{geometry}
\usepackage{nicefrac}
\usepackage[index,nonumber]{cuisine}
\usepackage[hidelinks]{hyperref}

%hide the numbering next to exch step
\usepackage{xpatch}
\makeatletter
\xpatchcmd{\Displ@ySt@p}{\arabic{st@pnumber}}{}{}{}
\makeatother

\hypersetup{
    pdftitle={Wenger Family Cookbook},
    pdfauthor={The Wenger Family},
    colorlinks=false,
    linktoc=all,
}
\includeonly{deserts,breads}

\begin{document}
    \title{Wenger Family Cookbook}
    \author{The Wenger Family}
    \maketitle

    \RecipeWidths{\textwidth}{0.5cm}{3cm}{3cm}{.75cm}{.75cm}
    \renewcommand*{\recipetitlefont}{\large\bfseries\sffamily}
    \renewcommand*{\recipequantityfont}{\sffamily\bfseries}
    \renewcommand*{\recipeunitfont}{\sffamily}
    \renewcommand*{\recipeingredientfont}{\sffamily}
    \renewcommand*{\recipefreeformfont}{\itshape}

    \newpage
    \tableofcontents
    \setcounter{tocdepth}{3}

"""

def docend():
    "returns the conclusion of the cookbook"

    return "\\end{document}\n"


def getchapter(chname):
    "returns the tex to start a chapter"

    return "\n\\newpage\n\\phantomsection\n\\section{%s}\n" % chname


def getsection(secname):
    "returns the tex to start a section"

    return "\\phantomsection\n\\subsection{%s}\n" % secname


def getrecipe(filename):
    "returns a recipe for the cookbook"

    try:
        filein = (open(filename)).read()
    except:
        print "unable to read from ", os.path.abspath(filename)

    return "\n\\phantomsection\n%s\n" % filein

ignoreddirs = ['.git']
outfilename = "wengercookbook.tex"

outfile = open(outfilename, 'w')
outfile.write(docstart())

# deal with chapters
chapters = [x for x in os.listdir('.') if (os.path.isdir(x) and x not in ignoreddirs)]
for chapter in chapters:
    os.chdir(chapter)
    sections = [x for x in os.listdir('.') if (os.path.isdir(x) and x not in ignoreddirs)]
    rootrecipes = [x for x in os.listdir('.') if (os.path.isfile(x) and x.endswith(".tex"))]
    if len(sections) > 0 or len(rootrecipes) > 0:
        outfile.write(getchapter(chapter))

    # deal with sections
    for section in sections:
        os.chdir(section)
        recipes = [x for x in os.listdir('.') if (os.path.isfile(x) and x.endswith(".tex"))]
        if len(recipes) > 0:
            outfile.write(getsection(section))
        for recipe in recipes:
            outfile.write(getrecipe(recipe))
        os.chdir("..")

    # if there are other recipes make an "others" section
    if len(rootrecipes)>0 and len(sections)>0:
        outfile.write(getsection("Others"))
    for recipe in rootrecipes:
        outfile.write(getrecipe(recipe))
    os.chdir("..")

outfile.write(docend())
outfile.close()
